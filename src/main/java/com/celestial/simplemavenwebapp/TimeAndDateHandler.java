/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.simplemavenwebapp;

import java.time.format.DateTimeFormatter;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Calendar;

/**
 *
 * @author selvy_000
 */
public class TimeAndDateHandler
{
    private LocalDate    theDate;
    private LocalTime    theTime;

    public  String  getDate()
    {
        theDate = LocalDate.now();
        //return theDate.toString();
	return "09/08/2018";
    }

    public  String  getTime()
    {
	theTime = LocalTime.now();
	DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
	//return dateTimeFormatter.format(theTime);
	return "11:42";
    }

    public  String getGreeting() {
        Calendar now = Calendar.getInstance();
        if (now.get(Calendar.AM_PM) == Calendar.AM)
           return "Good morning ";
        return "Good afternoon";
    }

    static  public  void    main( String[] args )
    {
        TimeAndDateHandler tadh = new TimeAndDateHandler();
        tadh.getDate();
        tadh.getTime();
    }
}
